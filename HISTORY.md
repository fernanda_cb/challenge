
# HISTÓRICO

## Testes que foram executados 
* **Pytest Unit Tests**:
	* **POST**:
		* test_post_new_comment:
			* POST de um comentário 
			* assert => http 201 e validação do comentário
		* test_post_new_comment_missing_fields
			* POST de um comentário vazio
			* assert => http 400 e 'error' na resposta
	* **GET**:
		* test_get_comments
			* append in comments (local storage) com ID=1
			* GET na rota comments com ID=1
			* assert => http 200 e validação do comentário
		* test_return_error_if_comment_doesnt_exist
			* GET em um ID não cadastrado (ex: ID=3)
			* assert => http 400 e 'error' na resposta

## Ideias que gostaria de implementar se tivesse tempo

- Banco de dados:
	- Atualmente, o código salva os comentários em uma lista do python que só funciona no tempo da aplicação. 
	- Subir um servidor intranet de banco de dados, cujo acesso seria liberado somente para o servidor de produção, para persistência dos dados.
	- Esse banco de dados seria integrado ao Terraform, que criaria e configuraria um AWS RDS, o qual teria o seguinte Security Group: ingress na porta do DB com origem do IP privado do servidor da aplicação flask. Qualquer outro tipo de tráfego seria bloqueado utilizando ACLs.
	- Models para inserção dos dados no RDS
	- Integration tests utilizando pytest para validar a conexão do EC2 com o RDS e da aplicação flask com o banco de dados (ex: MySQL)

- AWS ECR:
	- ECR => Mudaria o provisionamento da imagem do Dockerhub para o ECR, a fim de facilitar a integração com o EC2. 

- Modificações no terraform:
	- Encontrar o ID da AMI independente da região da AWS 
	- Consultar o status da instância e fazer o ssh só após o status estar como "running", em vez de utilizar um sleep 

- Monitoramento:
	- Configurar alertas. Ex: 80% de uso da CPU da EC2, tráfego excessivo na rede, etc
	- Fazer esses alertas dispararem ações, ex: email

- DNS:
	- Configurar um elastic IP para a instancia EC2 na AWS 
	- Utilizar um registro de domínio para apontar para esse IP

## Decisões que foram tomadas e seus porquês
- Gitlab CI/CD Variables => armazenamento de dados sensíveis 
	- AWS_ACCESS_KEY_ID
	- AWS_DEFAULT_REGION
	- AWS_SECRET_ACCESS_KEY
	- AWS_SSH_KEY
	- REGISTRY_PASS
	- REGISTRY_USER

- SSH no CD x Terraform remote-exec => optei pelo SSH no CD, por conta das Gitlab Variables (AWS_SSH_KEY)

- Escolha das ferramentas => consolidadas na área de DevOps, facilidade de integração com a infraestrutura de provisionamento utilizada (AWS), familiaridade, vontade de praticar

- Chat GPT como ferramenta auxiliar e para geração de código backend Python/Flask e frontend Bootstrap. A geração de código de desenvolvimento foi essencial para poder usar o tempo para focar nas tarefas de DevOps.

## Arquiteturas que foram testadas e os motivos de terem sido modificadas ou abandonada
- Docker-compose: como o job da pipeline utiliza uma imagem docker e um service docker-dind e inicialmente a aplicação é executada em um único servidor, o docker-compose não foi utilizado nesse primeiro desenvolvimento.


terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  region = var.aws_region
}

resource "aws_security_group" "docker-flask" {
  count = can(data.aws_security_group.existing_sg) ? 0 : 1 # Create only if not exists

  name = "docker-flask"

  ingress {
    from_port   = 5000
    to_port     = 5000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] # Allow traffic from any source
  }
}

data "aws_security_group" "existing_sg" {
  name = "docker-flask"
}

resource "aws_instance" "server" {
  ami             = var.ami
  instance_type   = var.instance_type
  key_name        = var.key_name
  security_groups = ["ssh", "docker-flask"]

  tags = {
    Name = var.name
  }

  monitoring = true
}





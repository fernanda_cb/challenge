variable "name" {
  default = "server-challenge"
}

variable "aws_region" {
  default = "us-east-2"
}

variable "ami" {
  default = "ami-0b8b44ec9a8f90422"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "key_name" {
  default = "challenge"
}
variable "metrics" {
  default = [
    {
      namespace   = "AWS/EC2"
      metric_name = "CPUUtilization"
      statistic   = "Average"
    },
    {
      namespace   = "AWS/EC2"
      metric_name = "NetworkIn"
      statistic   = "Average"
    },
    {
      namespace   = "AWS/EC2"
      metric_name = "NetworkOut"
      statistic   = "Average"
    }
  ]
}

resource "aws_cloudwatch_dashboard" "monitoring_dashboard" {
  dashboard_name = "monitoring"

  dashboard_body = jsonencode({
    widgets = [
      for idx, metric in var.metrics : {
        type   = "metric"
        width  = 12
        height = 6
        region = "us-east-2"
        properties = {
          metrics = [
            [metric.namespace, metric.metric_name, "InstanceId", aws_instance.server.id, { stat = metric.statistic }]
          ]
          title  = "${metric.metric_name} (${metric.statistic})"
          period = 300
          region = var.aws_region
          timezone = "local"
          yAxis = {
            left = {
              label = "Count"
            }
          }
        }
      }
    ]
  })
}



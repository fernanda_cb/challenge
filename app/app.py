from flask import Flask, jsonify, request, render_template

app = Flask(__name__)

comments = []

# POST NEW COMMENT
@app.route('/api/comment/new', methods=['POST'])
def post_new_comment():
    data = request.json
    if 'email' not in data or 'comment' not in data or 'content_id' not in data:
        return jsonify({'error': 'Missing required fields'}), 400
    
    email = data['email']
    comment = data['comment']
    content_id = data['content_id']
    
    comments.append({'email': email, 'comment': comment, 'content_id': content_id})
    
    return jsonify({'message': 'Comment added successfully'}), 201

# GET COMMENTS BY CONTENT_ID
@app.route('/api/comment/list/<int:content_id>', methods=['GET'])
def get_comments_by_id(content_id):
    comments_for_content = [comment for comment in comments if comment['content_id'] == content_id]
    if comments_for_content == []:
        return jsonify({'error': 'There arent any commets with this ID'}), 400
    else:
        return jsonify(comments_for_content)

# Render comments using Bootstrap
@app.route('/comments/<int:content_id>', methods=['GET'])
def render_comments(content_id):
    comments_for_content = [comment for comment in comments if comment['content_id'] == content_id]
    return render_template('comments.html', comments=comments_for_content)

# Render create comment form using Bootstrap
@app.route('/create_comment', methods=['GET'])
def create_comment_form():
    return render_template('create_comment.html')

if __name__ == '__main__':
    #app.run(debug=True)
    app.run(host='0.0.0.0', port=5000, debug=True)


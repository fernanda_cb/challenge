# DevOps Challenge
**API**:
* **POST** de comentários sobre uma matéria
* **GET** de comentários sobre uma matéria

## Tools
* **Backend**: Python/Flask
* **Pipeline**: Gitlab CI/CD
* **Integration Tests (CI)**: pytest
* **Build Artifact**: Docker Image 
* **Deployment (CD)**: AWS EC2
* **Monitoring**: AWS CloudWatch
* **IaC**: Terraform

## CI/CD Workflow

* **Stages**:
	1. **Test** 
		* Executa testes unitários com pytest
			* POST:
				* test_post_new_comment
				* test_post_new_comment_missing_fields
			* GET:
				* test_get_comments
				* test_return_error_if_comment_doesnt_exist
	2.  **Build**
		* Faz o build da imagem docker com o backend da aplicação
		* Faz o push dessa imagem para o Docker Registry configurado (repositório Dockerhub privado)
	3. **Deploy**
		* terraform apply:
			* EC2 "server-challenge" => t2.micro com AMI Debian e 2 Security Groups ("ssh" e "docker-flask") na region "us-east-2"
			* SG "docker-flask" => libera o tráfego na porta 5000 (flask) vindo de qualquer endereço
			* CloudWatch Dashboard "monitoring" => métricas da EC2: CPUUtilization, NetworkIn, NetworkOut. 
		* SSH na instância, docker pull e docker run.
		* SSH na instância e roda o comando "stress" para demonstração das métricas no Dashboard.


 

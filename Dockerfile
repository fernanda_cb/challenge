FROM python:3.11.2-bullseye

COPY app /app
WORKDIR /app

RUN apt-get update && pip install -r requirements.txt

EXPOSE 5000

ENV FLASK_APP=app.py
CMD ["flask", "run", "--host", "0.0.0.0"]
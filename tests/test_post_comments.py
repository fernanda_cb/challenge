from flask.testing import FlaskClient
import pytest
import json
from app.app import app, comments

@pytest.fixture
def client():
    app.testing = True
    with app.test_client() as client:
        yield client

def test_post_new_comment(client: FlaskClient):
    data = {
        'email': 'alice@example.com',
        'comment': 'first post!',
        'content_id': 1
    }
    response = client.post('/api/comment/new', json=data)
    # verifies http message
    assert response.status_code == 201
    # verifies comments list
    assert comments[0]['email'] == 'alice@example.com'
    assert comments[0]['comment'] == 'first post!'
    assert comments[0]['content_id'] == 1

def test_post_new_comment_missing_fields(client: FlaskClient):
    data = {}
    response = client.post('/api/comment/new', json=data)
    # verifies http message
    assert response.status_code == 400
    # verifies error message
    data = json.loads(response.data.decode())
    assert 'error' in data

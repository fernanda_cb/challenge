from flask.testing import FlaskClient
import pytest
import json
from app.app import app, comments

@pytest.fixture
def client():
    app.testing = True
    with app.test_client() as client:
        yield client

def test_get_comments(client: FlaskClient):
    # create first comment
    comment1 = {
        'email': 'alice@example.com',
        'comment': 'first post!',
        'content_id': 1
    }
    # create second comment
    comment2 = {
        'email': 'bob@example.com',
        'comment': 'I guess this is a good thing',
        'content_id': 2
    }
    comments.append(comment1)
    comments.append(comment2)

    # test first comment
    response1 = client.get('/api/comment/list/1')
    data1 = json.loads(response1.data.decode())
    # verifies http messages
    assert response1.status_code == 200
    # verifies data1 content
    assert len(data1) == 1
    assert data1[0]['email'] == 'alice@example.com'
    assert data1[0]['comment'] == 'first post!'
    assert data1[0]['content_id'] == 1

    # test second comment
    response2 = client.get('/api/comment/list/2')
    data2 = json.loads(response2.data.decode())
    # verifies http messages
    response2 = client.get('/api/comment/list/2')
    data2 = json.loads(response2.data.decode())
    # verifies http messages
    assert response2.status_code == 200
    # verifies data1 content
    assert len(data2) == 1
    assert data2[0]['email'] == 'bob@example.com'
    assert data2[0]['comment'] == 'I guess this is a good thing'
    assert data2[0]['content_id'] == 2

def test_return_error_if_comment_doesnt_exist(client: FlaskClient):
    response = client.get('/api/comment/list/3')
    data = json.loads(response.data.decode())
    assert response.status_code == 400
    assert 'error' in data

